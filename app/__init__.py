import os
from config import Config
from flask import Flask
import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
from .bp_user import bp as user_bp
from .bp_book import bp as book_bp
from .common.middleware import response
from .common.middleware import teardown_appcontext_middleware
from .common.middleware import after_request_middleware
from .common.middleware import before_request_middleware
from .common.database import init_db


def create_app(config_class=Config):

    #
    app = Flask(__name__)

    #
    app.config.from_object(config_class)

    #
    app.register_blueprint(user_bp, url_prefix='/api/user')
    app.register_blueprint(book_bp, url_prefix='/api/book')

    app.response_class = response.JSONResponse

    before_request_middleware(app=app)

    after_request_middleware(app=app)

    teardown_appcontext_middleware(app=app)

    response.json_error_handler(app=app)

    init_db()

    #
    if not app.debug and not app.testing:
        if app.config['MAIL_SERVER']:
            auth = None
            if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
                auth = (app.config['MAIL_USERNAME'],
                        app.config['MAIL_PASSWORD'])
            secure = None
            if app.config['MAIL_USE_TLS']:
                secure = ()
            mail_handler = SMTPHandler(
                mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                fromaddr='no-reply@' + app.config['MAIL_SERVER'],
                toaddrs=app.config['ADMINS'], subject='AACE-Network Failure',
                credentials=auth, secure=secure)
            mail_handler.setLevel(logging.ERROR)
            app.logger.addHandler(mail_handler)
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('logs/aace-network.log',
                                           maxBytes=10240, backupCount=10)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s '
            '[in %(pathname)s:%(lineno)d]'))
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('AACE-Network')

    return app
