from . import bp


@bp.route('', methods=['POST'])
def create_book():
    pass


@bp.route('/<book_id>', methods=['GET'])
def get_book(book_id):
    pass


@bp.route('/all', methods=['POST'])
def get_books():
    pass


@bp.route('/<book_id>', methods=['PUT'])
def update_book(book_id):
    pass


@bp.route('/<book_id>', methods=['DELETE'])
def delete_book(book_id):
    pass
