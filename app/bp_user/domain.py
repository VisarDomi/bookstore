from . import backend
from flask import jsonify


def create_user(user_data):
    user = backend.create_user(user_data)
    return user


def get_all_users():
    first_user = backend.get_all_users()
    return jsonify(email=first_user.email)
