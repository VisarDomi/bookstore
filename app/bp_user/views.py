from . import bp
from flask import request
from . import domain


@bp.route('', methods=['POST'])
def create_user():
    return domain.create_user(request.get_json)


@bp.route('/<user_id>', methods=['GET'])
def get_user(user_id):
    pass


@bp.route('/all', methods=['GET'])
def get_users():
    return domain.get_all_users()


@bp.route('/<user_id>', methods=['PUT'])
def update_user(user_id):
    pass


@bp.route('/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    pass
