from flask import Response, jsonify, request
from functools import singledispatch
from ..exceptions import JSONException, InvalidAPIRequest
from werkzeug.exceptions import NotFound


@singledispatch
def to_serializable(rv):
    pass


class JSONResponse(Response):

    @classmethod
    def force_type(cls, rv, environ=None):
        rv = to_serializable(rv)
        return super(JSONResponse, cls).force_type(rv, environ)


def json_error_handler(app):
    @app.errorhandler(JSONException)
    def handle_invalid_usage(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response

    @app.errorhandler(NotFound.code)
    def resource_not_found(error):
        msg = f'The requested URL `{request.path}` was '\
            'not found on the server.'
        response = jsonify(InvalidAPIRequest(message=msg).to_dict())
        response.status_code = NotFound.code
        return response
