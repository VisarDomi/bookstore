from sqlathanor import AttributeConfiguration, relationship

from sqlalchemy import Column, Integer, String, ForeignKey
# from sqlalchemy.orm import backref

from ..common.database import BaseModel


class User(BaseModel):
    __tablename__ = 'users'
    __serialization__ = [
        AttributeConfiguration(name='email',
                               supports_json=(True, True))
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String, unique=True)

    books = relationship('Book', backref='user', lazy='dynamic')

    def __repr__(self):
        return f"<User(email='{self.email}')>"


class Book(BaseModel):
    __tablename__ = 'books'
    __serialization__ = [
        AttributeConfiguration(name='title',
                               supports_json=(True, True)),
        AttributeConfiguration(name='author',
                               supports_json=(True, True))
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String)
    author = Column(String)

    user_id = Column(Integer, ForeignKey('users.id'))

    def __repr__(self):
        return f"<Book(title='{self.title}', author='{self.author}')>"
